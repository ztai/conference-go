from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO

def accountVO_update(ch, method, properties, body):
    print("UPDATE VO FUNCTION??????")
    content = json.loads(body)
    '''
    # for key in body:
    #     temp_AccVO[key] = body[key]
    # temp_AccVO["href"] = body["href"]
    '''
    email = content["email"]
    first_name = content["first_name"]
    last_name = content["last_name"]
    is_active = content["is_active"]
    updated = datetime.fromisoformat(content["updated"])

    if is_active:
        print("creating accountVO")
        AccountVO.objects.filter(updated__lt=updated).update_or_create(
            email=email,
            defaults={
                "first_name" : first_name,
                "last_name" : last_name,
                # "is_active" : is_active,
                "updated" : updated
        })
        print("new AccountVO!")
    else:
        AccountVO.objects.get(email=email).delete()


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.exchange_declare(exchange="account_info",exchange_type="fanout")
        result = channel.queue_declare(queue="account_update",exclusive=True)
        queue_name = result.method.queue

        channel.queue_bind(exchange="account_info", queue=queue_name)
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=accountVO_update,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
