from django.contrib import admin

from .models import Attendee, Badge, AccountVO, ConferenceVO


@admin.register(Attendee)
class AttendeeAdmin(admin.ModelAdmin):
    pass


@admin.register(Badge)
class BadgeAdmin(admin.ModelAdmin):
    pass

@admin.register(ConferenceVO)
class ConferenceVO(admin.ModelAdmin):
    list_display=(
        "id",
        "name",
        "import_href"
    )

@admin.register(AccountVO)
class AccountVO(admin.ModelAdmin):
    pass
