from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee, ConferenceVO,AccountVO
import json
from django.views.decorators.http import require_http_methods


class AttendeeListDecoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeDetailDecoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name", "company_name", "created", "conference"]
    encoders = {"conference": ConferenceVODetailEncoder()}
    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email)
        return {"has_account": len(count) > 0}

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    # result = {
    #     "attendees": [
    #         {"name": attendee.name, "href": attendee.get_api_url()}
    #         for attendee in Attendee.objects.all()
    #     ]
    # }
    """
    # return JsonResponse(result, safe=False)
    if request.method == "GET":
        return JsonResponse(
            {"attendees": Attendee.objects.filter(conference={conference_vo_id})}, encoder=AttendeeListDecoder
        )
    else:
        content = json.loads(request.body)
        try:
            # conference_href = f"/api/conferences/{conference_vo_id}/"
            conference_href = f"/api/conferences/{content['conference_id']}/"

            content["conference"] = ConferenceVO.objects.get(
                import_href=conference_href
            )
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conferenceVO id for new attendee"}, status=400
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(attendee, encoder=AttendeeListDecoder, safe=False)


def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.
    """
    # attendee = Attendee.objects.get(id=id)
    # result = {
    #     "email": attendee.email,
    #     "name": attendee.name,
    #     "company_name": attendee.company_name,
    #     "created": attendee.created,
    #     "conference": {
    #         "name": attendee.conference.name,
    #         "href": attendee.conference.get_api_url(),
    #     },
    # }
    # return JsonResponse(result, safe=False)
    if request.method == "GET":
        return JsonResponse(
            Attendee.objects.get(id=id),
            encoder=AttendeeDetailDecoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference_id" in content:
                content["conference_id"] = ConferenceVO.objects.get(
                    id=content["conference_id"]
                )
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conferenceVO id"}, status=400
            )
        Attendee.objects.filter(id=id).update(**content)
        return JsonResponse(
            Attendee.objects.get(id=id),
            encoder=AttendeeDetailDecoder,
            safe=False,
        )
