from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests

def get_conference_weather(city, state):
    response = requests.get("http://api.openweathermap.org/geo/1.0/direct",
        params={
            "appid":OPEN_WEATHER_API_KEY,
            "q":f"{city}, {state}, USA"
            })
    cords = response.json()
    lat = str(cords[0]["lat"])
    lon = str(cords[0]["lon"])
    weather_response =  requests.get("http://api.openweathermap.org/data/2.5/weather",
        params={
        "appid":OPEN_WEATHER_API_KEY,
        "lat": lat,
        "lon": lon,
        "units":"imperial"
        })

    weather = weather_response.json()
    desc = weather["weather"][0]["description"]
    temp = weather["main"]["temp"]
    # if weather == None:
    #     return None
    return {"description":desc,"temp": temp}

def get_photo(city,state):
    picture_get = requests.get(f"https://api.pexels.com/v1/search",headers={"Authorization":PEXELS_API_KEY}, params = {"query" : f"{city}, {state}"})
    return picture_get.json()["photos"][0]["src"]["original"]
